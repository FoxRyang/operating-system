00000000000i[     ] Bochs x86 Emulator 2.2.6
00000000000i[     ]   Build from CVS snapshot on January 29, 2006
00000000000i[     ] System configuration
00000000000i[     ]   processors: 1
00000000000i[     ]   A20 line support: yes
00000000000i[     ]   APIC support: no
00000000000i[     ] CPU configuration
00000000000i[     ]   level: 5
00000000000i[     ]   fpu support: yes
00000000000i[     ]   paging support: yes, tlb enabled: yes
00000000000i[     ]   mmx support: yes
00000000000i[     ]   sse support: no
00000000000i[     ]   v8086 mode support: yes
00000000000i[     ]   VME support: yes
00000000000i[     ]   3dnow! support: no
00000000000i[     ]   PAE support: no
00000000000i[     ]   PGE support: no
00000000000i[     ]   PSE support: yes
00000000000i[     ]   x86-64 support: no
00000000000i[     ]   SEP support: no
00000000000i[     ] Optimization configuration
00000000000i[     ]   Guest2HostTLB support: no
00000000000i[     ]   RepeatSpeedups support: no
00000000000i[     ]   Icache support: no
00000000000i[     ]   Host Asm support: yes
00000000000i[     ]   Fast function calls: no
00000000000i[     ] Devices configuration
00000000000i[     ]   NE2000 support: no
00000000000i[     ]   PCI support: no
00000000000i[     ]   SB16 support: no
00000000000i[     ]   USB support: no
00000000000i[     ]   VGA extension support:  
00000000000i[MEM0 ] allocated memory at 0x7fb047ad4010. after alignment, vector=0x7fb047ad5000
00000000000i[MEM0 ] 32.00MB
00000000000i[MEM0 ] rom at 0xf0000/65536 ('/usr/local/share/bochs/BIOS-bochs-latest')
00000000000i[MEM0 ] rom at 0xc0000/32768 ('/usr/local/share/bochs/VGABIOS-lgpl-latest')
00000000000i[CMOS ] Using local time for initial clock
00000000000i[CMOS ] Setting initial clock to: Thu Sep 10 15:31:09 2015 (time0=1441884669)
00000000000i[DMA  ] channel 4 used by cascade
00000000000i[DMA  ] channel 2 used by Floppy Drive
00000000000i[FDD  ] tried to open '/dev/fd0' read/write: No such file or directory
00000000000i[FDD  ] tried to open '/dev/fd0' read only: No such file or directory
00000000000i[FDD  ] tried to open 'b.img' read/write: No such file or directory
00000000000i[FDD  ] tried to open 'b.img' read only: No such file or directory
00000000000i[VGA  ] interval=300000
00000000000i[     ] init_mem of 'harddrv' plugin device by virtual method
00000000000i[     ] init_mem of 'keyboard' plugin device by virtual method
00000000000i[     ] init_mem of 'serial' plugin device by virtual method
00000000000i[     ] init_mem of 'parallel' plugin device by virtual method
00000000000i[     ] init_mem of 'extfpuirq' plugin device by virtual method
00000000000i[     ] init_mem of 'speaker' plugin device by virtual method
00000000000i[     ] init_dev of 'harddrv' plugin device by virtual method
00000000000i[HD   ] HD on ata0-0: '30M.sample' 'flat' mode 
00000000000p[HD   ] >>PANIC<< ata0-0: could not open hard drive image file '30M.sample'
00000000000i[SYS  ] Last time is 1441884669
00000000000i[NGUI ] bx_nogui_gui_c::exit() not implemented yet.
00000000000i[     ] restoring default signal behavior
